<?php

/**
 * @file
 * File containing validation and submit callbacks.
 */

/**
 * Form validation callback for the date popup settings.
 * 
 * @param array $form       The form.
 * @param array $form_state The form's state.
 *
 * @return void
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function date_popup_allowed_dates_settings_validate($form, &$form_state) {
  $settings = &$form_state['values']['instance']['settings']['date_popup_allowed_dates'];

  $settings['min_date'] = trim($settings['min_date']);
  $settings['max_date'] = trim($settings['max_date']);

  $settings['stop_time_for_today']['hour'] = intval($settings['stop_time_for_today']['hour']);
  $settings['stop_time_for_today']['minute'] = intval($settings['stop_time_for_today']['minute']);

  if ($settings['stop_time_for_today']['hour'] < -1 || $settings['stop_time_for_today']['hour'] > 23) {
    form_set_error('stop_time_for_today][hour', t('Invalid hour value.'));
  }
  if ($settings['stop_time_for_today']['minute'] < 0 || $settings['stop_time_for_today']['minute'] > 55) {
    form_set_error('stop_time_for_today][minute', t('Invalid minute value.'));
  }
  $diff = array_diff(array_keys($settings['allowed_weekdays']), array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'));
  if (! empty($diff)) {
    form_set_error('allowed_weekdays', t('Allowed weekdays contains invalid values.'));
  }
  $diff = array_diff(array_keys($settings['min_date_skip_days']), array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'));
  if (! empty($diff)) {
    form_set_error('min_date_skip_days', t('Skip days contains invalid values.'));
  }

  $settings['hide_if_no_prev_next'] = (bool) $settings['hide_if_no_prev_next'] ? 1 : 0;
  $settings['constrain_input'] = (bool) $settings['constrain_input'] ? 1 : 0;
}

/**
 * Element validation callback.
 * 
 * @param array $form       The form.
 * @param array $form_state The form's state.
 *
 * @return void
 * @see date_popup_allowed_dates_date_popup_process_alter() in date_popup_allowed_dates.module.
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 **/
function date_popup_allowed_dates_date_picker_element_validate($element, &$form_state, $form) {
  if (date_hidden_element($element)) {
    return;
  }
  // Make sure that this is a date_popup field. It should already have been 
  // checked when this callback was added to this element, but whatever.
  if (isset($element['#instance']['widget']['type']) && 'date_popup' === $element['#instance']['widget']['type']) {
    if (! empty($element['#instance']['settings']['date_popup_allowed_dates'])) {
      $settings = $element['#instance']['settings']['date_popup_allowed_dates'];
      // No need to do any extra validation if no module specific settings are set.
      if (! empty($settings)) {
        # module_load_include('inc', 'date_api', 'date_api_elements');
        # date_popup_add();

        // Get the input value.
        $input_exists = NULL;
        $input = drupal_array_get_nested_value($form_state['values'], $element['#parents'], $input_exists);
        # $value = $element['#value']['date']; // This seems to work too?

        // Create a DateTime object from the date format and the passed value.
        $date = new DateObject($input, NULL, $element['#date_format']);

        // Minimum Date.
        $date_string = date_popup_allowed_dates_transform_relative_date_string($settings['min_date']);
        if ($date_string && FALSE !== ($timestamp = strtotime($date_string))) {
          $min_date = new DateObject($timestamp);
          $min_date->setTime(0,0,0);

          // Check minimum date.
          $diff = $min_date->diff($date);
          if ($diff->invert && $diff->days > 0) {
            form_error($element, t('You need to choose a later date than %date.', array('%date' => $min_date->sub(new DateInterval('P1D'))->format($element['#date_format']))));
          }
        }
        else {
          $min_date = new DateObject();
        }

        // Maximum Date.
        $date_string = date_popup_allowed_dates_transform_relative_date_string($settings['max_date']);
        if ($date_string && FALSE !== ($timestamp = strtotime($date_string))) {
          $max_date = new DateObject($timestamp);
          $max_date->setTime(0,0,0);

          // Check maximum date.
          $diff = $date->diff($max_date);
          if ($diff->invert && $diff->days > 0) {
            form_error($element, t('You need to choose an earlier date than %date.', array('%date' => $max_date->add(new DateInterval('P1D'))->format($element['#date_format']))));
          }
        }

        // If allowed weekdays settings has ben set, check if the weekday is valid.
        $allowed_weekdays = array_keys(array_filter($settings['allowed_weekdays']));
        if (! empty($allowed_weekdays)) {
          $weekday = strtolower(date("l", $date->getTimestamp()));
          if (! in_array($weekday, $allowed_weekdays)) {
            form_error($element, t('You need to choose one of the allowed weekdays.'));
          }
        }

        // Validate Stop Time.
        if (isset($settings['stop_time_for_today']['hour']) && intval($settings['stop_time_for_today']['hour']) >= 0) {
          $date_array = $date->toArray();
          $min_date_array = $min_date->toArray();
          // Make sure it's only checked for the earliest date according to $min_date.
          if ($date_array['day'] == $min_date_array['day'] && $date_array['month'] == $min_date_array['month'] && $date_array['year'] == $min_date_array['year']) {
            $date_now = new DateObject();
            $date_now_array = $date_now->toArray();
            // If the current time is later than the stop time...
            if ($date_now_array['hour'] >= $settings['stop_time_for_today']['hour'] && $date_now_array['minute'] >= $settings['stop_time_for_today']['minute']) {
              form_error($element, t('You need to choose one of the allowed weekdays.'));
            }
          }
        }
      }
    }
  }
}


