/*!
 * Date Popup Allowed Dates
 *
 * @author Hampus Nordin <nordin.hampus@gmail.com>
 */
;(function($, window, document, undefined) {
Drupal.behaviors.date_popup_allowed_dates = {
  attach: function (context) {
	  for (var id in Drupal.settings.datePopupAllowedDates.datepickers) {
	    $('#'+ id).bind('focus', Drupal.settings.datePopupAllowedDates.datepickers[id], function(e) {
				var $this = $(this);
	      if (!$this.hasClass('date-popup-allowed-dates-init')) {
	        var datePopupAllowedDates = e.data;
				
					for (var s in datePopupAllowedDates.settings) {
						$this.datepicker('option', s, datePopupAllowedDates.settings[s]);
					}
				
					for (var f in datePopupAllowedDates.functions) {
						var func = Drupal.behaviors.date_popup_allowed_dates.helpers.getFuncCallback(datePopupAllowedDates.functions[f], datePopupAllowedDates);
						
						if (typeof func === "function") {
							$this.datepicker('option', f, func);
						}
					}
				
					$(this).addClass('date-popup-allowed-dates-init');
	      }
	    });
	  }

		Drupal.settings.datePopupAllowedDates.functions = Drupal.settings.datePopupAllowedDates.functions || {};
		Drupal.settings.datePopupAllowedDates.callbacks = Drupal.settings.datePopupAllowedDates.callbacks || {};

		if (typeof Drupal.settings.datePopupAllowedDates.callbacks.beforeShowDay === "undefined") {
			Drupal.settings.datePopupAllowedDates.callbacks.beforeShowDay = function(da) {
				var d = da.getDay(),
            helpers = Drupal.behaviors.date_popup_allowed_dates.helpers,
            now,
            minDate,
						ds = Drupal.settings.datePopupAllowedDates.datepickers[this.id],
            minDays = ds.dpadSettings.minDays,
						ad = ds.dpadSettings.allowedDays,
						st = ds.dpadSettings.stopTimeForToday,
            minDateSkipDays = ds.dpadSettings.minDateSkipDays;

				// Allowed Days
				if (ad.length) {
					for (var i = 0; i < ad.length; i++) {
						if (jQuery.inArray(d, ad) != -1) {
							return [false];
						}
					}
				}

				// Stop Time for Today
				if (parseInt(st.hour) >= 0) {
					var stDate = (ds.settings.minDate) ? Date.parse(ds.settings.minDate) : new Date();
					if (da.getUTCDate() == stDate.getUTCDate() && da.getUTCMonth() == stDate.getUTCMonth() && da.getUTCFullYear() == stDate.getUTCFullYear()) {
            now = new Date();
            if (now.getHours() >= st.hour && now.getMinutes() >= st.minute) {
							return [false];
						}
					}
				}
				return [true];
			};
		}
	},
	"helpers": {
		getFuncCallback: function(funcName, settings) {
			var callbackName = funcName.split(".").pop(),
					callback;
										
			if (typeof callbackName === "string" && callbackName) {
				callback = settings.callbacks[callbackName] || Drupal.settings.datePopupAllowedDates.callbacks[callbackName];
				
				if (typeof callback === "function") {
					return callback;
				}
			}
			return false;
		},
    getCalculatedMinDate: function(currentDate, minDays, skipDates) {
      var skipDatesLeftThisWeek = 0,
          currentDay = currentDate.getDay();

      for (var i = skipDates.length - 1; i >= 0; i--) {
        if (this.convertFirstDayToMonday(currentDay) <= this.convertFirstDayToMonday(skipDates[i])) {
          skipDatesLeftThisWeek++;
        }
      }
    },
    convertFirstDayToMonday: function (d) {
      return (d + 6) % 7;
    }

	}
};
})(jQuery, this, this.document);