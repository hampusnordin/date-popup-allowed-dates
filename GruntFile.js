module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*!\n * <%= pkg.name %> <%= pkg.version %>\n *\n * <%= pkg.description %>\n *\n * Author: <%= pkg.author %>\n * Last built: <%= grunt.template.today("yyyy-mm-dd") %>\n */\n'
      },
      build: {
        src: 'assets/js/date_popup_allowed_dates.js',
        dest: 'assets/js/date_popup_allowed_dates.min.js'
      }
    },
    sass: {
      dist: {
        options: {
          style: 'nested',
          require: ['./ruby/timestamp.rb']
        },
        files: {
          'assets/css/admin.css': 'assets/sass/admin.scss'
        }
      }
    },
    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass']
      },
      js: {
        files: 'assets/js/date_popup_allowed_dates.js',
        tasks: ['uglify']
      }
    }
  });

  // Uglify
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', ['uglify']);

  // Sass
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',['watch']);
};
